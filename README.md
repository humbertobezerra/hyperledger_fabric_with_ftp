- docker swarm init
- docker network create -d overlay --attachable fabric
- ./run_ftp.sh
- docker stack deploy -c swarm_fabric.yaml my_stack

Join Cli 

- cryptogen generate --config=crypto-config.yaml # generate crypto material
- configtxgen -profile OneOrgOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
- configtxgen -profile OneOrgChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID naq
- configtxgen -profile OneOrgChannel -outputAnchorPeersUpdate ./channel-artifacts/Org1MSPanchors.tx -channelID naq -asOrg Org1MSP
- peer channel create -o orderer.example.com:7050 -c naq -f /tmp/channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile /etc/hyperledger/fabric/crypto-config/ordererOrganizations/example.com/tlsca/tlsca.example.com-cert.pem
- peer channel join naq.block
- peer channel fetch 0 -c naq --tls $CORE_PEER_TLS_ENABLED --cafile /etc/hyperledger/fabric/crypto-config/ordererOrganizations/example.com/tlsca/tlsca.example.com-cert.pem --orderer orderer.example.com:7050
- mv true na.block
- peer channel join -b naq.block --tls $CORE_PEER_TLS_ENABLED --cafile /etc/hyperledger/fabric/crypto-config/ordererOrganizations/example.com/tlsca/tlsca.example.com-cert.pem --orderer orderer.example.com:7050
